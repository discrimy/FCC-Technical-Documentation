# FCC Technical Documentation
Example of documentation page with static navigation bar and sections. This project uses HTML and pure CSS.

https://discrimy.gitlab.io/FCC-Technical-Documentation/

> Created during **[freeCodeCamp](https://freecodecamp.org)** learning from scratch
